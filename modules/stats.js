let hash=null;
let scores=null;

function getHash() {
    url=`backend/getHash.php?world=${seed}`;
    fetch(url).then(
        function (response){
          return response.text();
        }).then(
            function (text){
                let obj=JSON.parse(text);
                if (obj.hash) {
                  hash=obj.hash;
                  getHighs(hash);
                }
            }
        );
}

function postHigh(time,distance,name){
    console.log("posting High");
    const formData = new FormData;
    formData.append('hash',hash);
    formData.append('world',seed);
    formData.append('time',courseTime);
    formData.append('distance',courseDistance);
    formData.append('name',moniker.substring(0,2));
    fetch('backend/updateHighs.php', {
        method: 'POST',
        body: formData,
      });
}

function getHighs(hash){  // Untested
    const formData = new FormData;
    formData.append('hash',hash);
    formData.append('world',seed);
    fetch(`backend/getHighs.php`, {
        method: 'POST',
        body: formData
      }).then(
        function(response) {
            return response.text();
        }).then(
        function (text){
            let obj=JSON.parse(text);
            scores=obj.scores;
            showHighs(scores);
        }
        );
}

function showHighs(scores){
  let table="<table><tbody><tr><th>Time</th><th>Distance</th><th>Name</th></tr>";
  scores.forEach(function (e){
    table+=`<tr><td>${e.time}</td><td>${e.distance}</td><td>${e.name}</td></tr>`;
  });
  table+="</tbody></table>";
  document.getElementById('scores').innerHTML=table;
}

if (hash==null) { getHash(); }
