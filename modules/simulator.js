// From code behind
//https://org.coloradomesa.edu/~kcastlet/darpa/page4.html
// Converted to Javascript for Ludum Dare 53

const leftbuf=20;

const sizeX=800;
const sizeY=600;
const feetperpixel=12.0/40.0; // 12 feet is 40 pixels
const done=false;
const finish=false;
const controlset=false;
  
class Course{
constructor() {
    this.intwaypoints=[];
    this.obstacles=[];
    this.waypoints=[];
    }
    generateWaypoints() {
    // Don't have code for this
    }
    generatorObstacles(numobs) {
        for (let j=0;j<numobs;j++) {
            let x=(float)(rand() % (maxx-minx)+minx);
            let y=(float)(rand() % (maxy-miny)+miny);
            let h;
            if (height<0.0) h=(float)(rand() % 1010)/1000.0;
            else h=height;
            obstacles.push(o);
        }
    }
};
  
let course=new Course();


class Vehicle {
    reset() {
        this.which=0;
        this.speed=0.0;
        this.angle=0.0;
        this.heading=0.0;
        this.x=0.0;
        this.y=0.0;
        this.playing=false;
        this.first=true;
        this.lcount=0;
        for (let i=0;i<leftbuf;i++) this.lefts[i]=0.0;
        this.left=0.0;
        this.lcount=0;
        this.sense=0.5;
        this.avoid=0.5;
        this.dir  =0.5;
        this.cross=0.5;
        this.blend=0.5;
    }
    constructor(){
        this.lefts=new Array(leftbuf);
        this.reset();
    }
    getPosition() { return this.position; }
    getHeading() {return this.heading; }
    getSense() {return this.sense; }
    setLeft(newleft) { 
        left=left+newleft-lefts[lcount];
        lefts[lcount]=newleft;
        lcount=(lcount+1) % leftbuf;
    }
    steeringAt(newAngle) {this.angle=newAngle; }
    speedAt(newSpeed) {this.speed=newSpeed; }
    turnLeft() { if (this.angle>-45.0) this.angle-=5.0; }
    turnRight() { if (this.angle<45.0)  this.angle+=5.0; }
    faster() {if (this.speed<60.0) this.speed+=1.0;}
    slower() {if (this.speed>0.0) this.speed-=1.0; }
    update(millis) {
        if (speed>0.999) { 
            if (sound) {
                // turn on car sound
                playing=true;
            }
            let feetpermsec=speed*.00146667; // mph->feet per millisecond
            this.x+=feetpermsec*(millis)*Math.sin(heading);
            this.y+=feetpermsec*(millis)*Math.cos(heading);
            this.heading+=feetpermsec*(millis)*(Math.tan(this.angle*3.14159/180.0)/12);
            if (this.heading<0.0) this.heading+=2*3.14159;
            if (this.heading>2.*3.14159) this.heading-=2*3.14159;
        }
        else {
        if (sound) {
            // turn off car sound
            playing=false;
        }
        }  
    }
    diffAngle(c,theta) {
        let dy1=sin(theta);
        let dx1=cos(theta);
        let dx2=c.waypoints[this.which+1].x-c.waypoints[this.which].x;
        let dy2=c.waypoints[this.which+1].y-c.waypoints[this.which].y;
        return Math.acos((dx1*dy2+dx2*dy1)/(Math.sqrt(dx1*dx1+dy1*dy2)*
                                        Math.sqrt(dx2*dx2+dy2*dy2)));
    }
    findSegAngle(c) {
        let segangle;
        let ex=c.waypoints[this.which+1].x;
        let sx=c.waypoints[this.which].x;
        let ey=c.waypoints[this.which+1].y;
        let sy=c.waypoints[this.which].y;
        let dx=ex-sx;
        let dy=ey-sy;
        if (dx>-0.1 && dx<0.1) {
            if (dy>0.0) segangle=0.0;
            else segangle=180.0;
        } else {
                segangle=3.14159/2-atan(dy/dx);
                if (dx<0.0) segangle+=3.14159;
            segangle=segangle*180.0/3.14159;
        }
        return segangle;
    }

    findSpeed(c) { return c.waypoints[this.which].h; }

    crossTrack(c) {
        let x0=c.waypoints[this.which].x;
        let y0=c.waypoints[this.which].y;
        let x1=c.waypoints[this.which+1].x;
        let y1=c.waypoints[this.which+1].y;
        let x2=this.x;
        let y2=this.y;
        let area2=((x1-x0)*(y0+y1)+(x2-x1)*(y2+y1)+(x0-x2)*(y0+y2));
        let base=sqrt((x1-x0)*(x1-x0)+(y1-y0)*(y1-y0));                 
        if (base==0.0) return 0.0;
        else if ((x1-x0)*(x2-x0)+(y1-y0)*(y2-y0)>0.0) return -area2/base;
        else return area2/base;
    }
    drive(c) {
        let sugangle;
        if (this.which==0 && this.first) {
            this.sugspeed=this.findSpeed(c);
            this.segangle=this.findSegAngle(c);
            this.first=false;
        }
        if (this.which+1>=c.waypoints.length()) {
            reset();
            this.finish=true;
        }
        if ((fabs(c.waypoints[this.which+1].x-x)+
                fabs(c.waypoints[this.which+1].y-y))<15){ 
                this.which++;
                this.sugspeed=this.findSpeed(c); // speed
                this.segangle=this.findSegAngle(c);
        }
        if (sugspeed>speed) this.speed++;
        else this.speed--;
        let degheading=heading*180.0/3.14159;  	
        let headingerror;
        if (segangle<90 && degheading>270) headingerror=(360+segangle)-degheading;
        else if (segangle>270 && degheading<90) headingerror=segangle-(360+degheading); 
        else headingerror=segangle-degheading;
        sugangle     =blend*(dir*headingerror+10.0*cross*crossTrack(c))+
                        (1.0-blend)*-left*500.0*avoid/speed;
        if (sugangle>45.0) sugangle=45.0;
        else if (sugangle<-45.0) sugangle=-45.0;
        if (90+sugangle<90+angle) angle-=7.5;
        else angle+=7.5;
    }
};
  
  //let vehicle=new Vehicle();

  let last=0;
function mainThread() {    
    while (!done) {
     /*   next=SDL_GetTicks(); */
        let next=51;
        if (last>next) last=0;
        vehicle.update(next-last);
        elasped+=(next-last);
        if(elasped>50 && !control) { // Every 50ms drive
            vehicle.drive(course);
            elasped-=50;
        }
    }
}    
