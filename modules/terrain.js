import * as THREE from '../js/three.module.js';
import * as Common from './common.js';
import * as TerrainVS from './terrain.vs.js';
import * as TerrainFS from './terrain.fs.js';

// Terrain generation from old demo project
// https://org.coloradomesa.edu/~kcastlet/CSCI445/demos/eighth/
let points=[];
let triangles=[];
	function position(x,y,z,error) {  // Look for duplication midpoints
		for (let j=0;j<points.length;j++) {
		if (Math.abs(x-points[j].x)<error &&
			/*Math.abs(y-points[j].y)<error &&*/
			Math.abs(z-points[j].z)<error) 
			return j
		}
		return -1;
	}
	function distance(pa,pb) { // x and z are two axis more perdendicular to screen
		return Math.sqrt(Math.pow((pb.x-pa.x),2)+Math.pow((pb.z-pa.z),2));
	}
	function nearest(p) {
	  let retval=points[0];
	  let dn=distance(p,points[0]);
	  for (let i=1;i<points.length;i++){
		let d=distance(p,points[i]);
		if (d<dn) {
			dn=d;
			retval=points[i];
		}
	  }
	  return retval.y;
	}
	function addMidpoint(P1Index,P2Index,De) {
		let Xm=(points[P1Index].x+points[P2Index].x)/2;
		let Ym=(points[P1Index].y+points[P2Index].y)/2 +Common.fprng()*De-0.5*De;
		let Zm=(points[P1Index].z+points[P2Index].z)/2;
		let index=position(Xm,0.0,Zm,1.);  //If not already set
		if (index<0) {
		  index=points.length;
		  points[index]={x:Xm,y:Ym,z:Zm};
		}
		return index;
	}
	function recursiveGenerate(P1Index,P2Index,P3Index,RMax,De) {
		if (RMax>1.0) {
			var p1p2m=addMidpoint(P1Index,P2Index,De);
			var p2p3m=addMidpoint(P2Index,P3Index,De);
			var p1p3m=addMidpoint(P1Index,P3Index,De);
			recursiveGenerate(P1Index,p1p2m,p1p3m,RMax/2,De/2.0);
			recursiveGenerate(P2Index,p2p3m,p1p2m,RMax/2,De/2.0);
			recursiveGenerate(P3Index,p1p3m,p2p3m,RMax/2,De/2.0);
			recursiveGenerate(p1p2m,p2p3m,p1p3m,RMax/2,De/2.0);			     
		} else
		  triangles[triangles.length]=[P1Index,P2Index,P3Index];
	} 
	function generateElevations(de) {
		points[0]={x:-100,y:0,z:-100};
		points[1]={x:100,y:0,z:-100};
		points[2]={x:100,y:0,z:100};
		points[3]={x:-100,y:0,z:100};
		recursiveGenerate(0,1,2,64,de);
		recursiveGenerate(0,2,3,64,de);
	}
	function renderElevations(maxE) {
		generateElevations(maxE);
		let g=new THREE.BufferGeometry();
		let vertices=[];
		let indices=[];
		for (let i=0;i<points.length;i++) 
			vertices=vertices.concat([points[i].x,points[i].y,points[i].z]);
		for (let i=0;i<triangles.length;i++) 
			indices=indices.concat([triangles[i][2],triangles[i][1],triangles[i][0]]);
		g.setIndex(indices);
		g.setAttribute('position',new THREE.BufferAttribute(new Float32Array(vertices),3));
		return g;
	}

function generate(scene,maxE) {
  let geometry=renderElevations(maxE);
  let mat=Common.getMaterial('terrain');
  let mesh = new THREE.Mesh( geometry, mat );
  scene.add( mesh );
}

function update(dt) {
  //mesh.rotation.x+=dt;
}


export {generate,nearest,update};
