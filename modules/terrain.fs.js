import * as Common from './common.js';

Common.addFS("terrain",Common.frag`
precision mediump float;

varying vec3 normalInterp;
varying vec3 vertPos;
varying vec3 newPosition;

uniform int mode;
uniform vec3 lightRPos;
uniform vec3 lightLPos;
uniform float time;
uniform vec3 water;
uniform vec3 beach;
uniform vec3 grass;
uniform vec3 forrest;
uniform vec3 rock;
uniform vec3 snow;
uniform vec3 lightColor;

const float shininess = 1.0;

void main() {
	vec3 normal = normalize(normalInterp);
	vec3 lightLDir = normalize(lightLPos - vertPos);
	vec3 lightRDir = normalize(lightRPos - vertPos);

	float specularL = 0.0;
	float specularR = 0.0;
	float specAngleL =0.0;
	float specAngleR =0.0;
	float tod=cos(time/5.)*0.5+0.5;
	vec3 viewDir = normalize(-vertPos);
	vec3 reflectLDir = reflect(-lightLDir, normal);
	vec3 reflectRDir = reflect(-lightRDir, normal);
	specAngleL = max(dot(reflectLDir, viewDir), 0.0);
	specAngleR = max(dot(reflectRDir, viewDir), 0.0);
	specularL = pow(specAngleL, shininess/4.0);
	specularR = pow(specAngleR, shininess/4.0);
	vec3 ambientColor = snow;
	if (newPosition.y<0.0) ambientColor=water/2.;
	else if (newPosition.y<5.0) ambientColor=beach/2.;
	else if (newPosition.y<25.0) ambientColor=grass/2.;
	else if (newPosition.y<50.0) ambientColor=forrest/2.;
	else if (newPosition.y<60.0) ambientColor=rock/2.;
	vec3 colorLinear = ambientColor*tod +
						specularL * lightColor + specularR*lightColor;
	gl_FragColor = vec4(colorLinear,1.0);
}
`);
