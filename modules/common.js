import * as THREE from '../js/three.module.js';

const frag=x=>String(x);
const vert=x=>String(x);

// Need a PRNG that is seedable.
// Use the URL after ? as seed for course (and index into high scores)
let args=window.location.href.split('?');
var seed; 
if (args.length<2) seed=0;
else seed=Number(args[1]);

// https://stackoverflow.com/questions/521295/seeding-the-random-number-generator-in-javascript
// https://en.wikipedia.org/wiki/Nothing-up-my-sleeve_number
var prng = sfc32(0x9E3779B9, 0x243F6A88, 0xB7E15162, seed);

function fprng() { return prng()/ 4294967296;}

let maxE=Math.log(seed+1)*3.;
if (maxE<10) maxE=20;

const courseTime=0.0;
const courseDistance=0.0;
const moniker="KJC";

// Three js is needed in the set of functions
// Really this is just media managers for textures and shaders
// Hoping to use just shaders and not textures
// getting a material or texture repeatedly will just get a reference

var uniforms= {
	time:{ value:1.0},
	lightRPos:{value:new THREE.Vector3(-10,0,10)},
	lightLPos:{value:new THREE.Vector3(10,0,10)},
	lightColor:{value:new THREE.Vector3(.8,.7,.5)},
	tod: {value:1.0 },
	hatch:{value:new THREE.Vector3(30,20,10)},
	thick:{value:1},
	water:{value:new THREE.Vector3(0.55,0.63,.73)},
    beach:{value:new THREE.Vector3(0.72,0.65,0.63)},
    grass:{value:new THREE.Vector3(0.37,0.37,0.21)},
    forrest:{value:new THREE.Vector3(0.19,0.2,0.12)},
    rock:{value:new THREE.Vector3(0.66,0.45,0.33)},
    snow:{value:new THREE.Vector3(0.22,0.16,0.15)},
	lightColor:{value:new THREE.Vector3(0.5, 0.5, 0.1)}
};

const materials=new Map;
const vshaders={};
const fshaders={};
function getMaterial(name) {
	//console.log(vshaders);
	//console.log(fshaders);
	if (!materials.has(name)) {
		let mat=new THREE.ShaderMaterial(
			{uniforms:uniforms,
			 vertexShader:vshaders[name],
			 fragmentShader:fshaders[name]});
		  materials.set(name,mat);
		  return mat;
		} else
	  return materials.get(name);
}

function sfc32(a, b, c, d) {
    return function() {
      a >>>= 0; b >>>= 0; c >>>= 0; d >>>= 0; 
      var t = (a + b) | 0;
      a = b ^ b >>> 9;
      b = c + (c << 3) | 0;
      c = (c << 21 | c >>> 11);
      d = d + 1 | 0;
      t = t + d | 0;
      c = c + t | 0;
      return (t >>> 0);
    }
}

/* Perlin from https://en.wikipedia.org/wiki/Perlin_noise*/ 
function lerp(a0,a1,w) { 
  return (a1-a0)*w+a0; 
}
function lerp2(a0,a1,w) {
  let lx=lerp(a0.x,a1.x,w);
  let lz=lerp(a0.z,a1.z,w)
  return {x:lx,z:lz};
}
function randomGradient(x,y) {
  let d=Math.sqrt(x*x+y*y);
  if (d==0)d=1;
  let a=2.0*Math.PI*(fprng()*x+fprng()*y)/d;
  return {x:Math.cos(a),y:Math.sin(a)};
}
function dot(ix,iy,x,y){
  let g=randomGradient(x,y);
  let dx=x-ix;
  let dy=y-iy;
  return dx*g.x+dy*g.y;
}
function perlin(x,y) {
	let x0=Math.floor(x);
	let x1=x0+1;
	let y0=Math.floor(y);
	let y1=y0+1;
	let sx=x-x0;
	let sy=y-y0;
    let n0=dot(x0,y0,x,y);
	let n1=dot(x1,y0,x,y);
	let ix0=lerp(n0,n1,sx);
	n0=dot(x0,y1,x,y);
	n1=dot(x1,y1,x,y);
	let ix1=lerp(n0,n1,sx);
	return lerp(ix0,ix1,sy)*.5+.5;
}

function addVS(name,code){
	vshaders[name]=code;
}

function addFS(name,code) {
	fshaders[name]=code;
}

export {maxE,prng,fprng,getMaterial,lerp2,seed,uniforms,addVS,addFS,frag,vert};
