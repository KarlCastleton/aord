import * as THREE from '../js/three.module.js';
import * as Common from './common.js';
import * as Terrain from './terrain.js';

// Three js is needed in the set of functions
class Tumbleweed {
	constructor(scene,x,y,z,size) {
		this.size=size;
		let geometry=new THREE.SphereGeometry(this.size,16,8);
	    let canvas=document.getElementById('weedcanvas');
	    Tumbleweed.draw(canvas);
		let texture = new THREE.CanvasTexture(canvas);
        let mat = new THREE.MeshBasicMaterial({map: texture});
		this.item=new THREE.Mesh(geometry,mat);
		let v=new THREE.Vector3(x,0.,z);
		//let e=nearest(v);
		v.y=Terrain.nearest(v);
		this.item.position.set(v.x,v.y+this.size/2+.5,v.z);
		this.item.transparency=true;
		this.ds=Common.prng()%100/10000.;
		scene.add(this.item);
	}
	static drawn=false;
	static draw(canvas) {
		if (!Tumbleweed.drawn) {
			let ctx=canvas.getContext('2d');
			ctx.fillStyle = "#1f1f1f";
			ctx.fillRect(0, 0, 256, 256);
			Tumbleweed.weedDrawn=true;
			console.log("Starting Weed Loop");
			let q=[];
			let r=32;
			for (let a=0.;a<Math.PI*2.0;a+=Math.PI/5.0) {
				let nx=r*(Math.cos(a)+Math.sin(a));
				let ny=r*(-Math.sin(a)+Math.cos(a));
				q.push({xs:0,ys:0,xe:nx,ye:ny,w:15});
			}
			let div=Common.prng()%180;
			let da=15.*Math.PI/div;
			let dr=0.5;
			while (q.length>0 ) {
				let p=q.shift();
				ctx.strokeStyle='#2f2f2f';
				ctx.lineWidth = p.w;
				ctx.beginPath(); 
				ctx.moveTo(p.xs+128+1,p.ys+128+1);
				ctx.lineTo(p.xe+128+1,p.ye+128+1);
				ctx.stroke();
				ctx.strokeStyle='tan';
				ctx.lineWidth = p.w;
				ctx.beginPath();
				ctx.moveTo(p.xs+128,p.ys+128);
				ctx.lineTo(p.xe+128,p.ye+128);
				ctx.stroke();
				let dx=p.xe-p.xs;
				let dy=p.ye-p.ys;
				let r=Math.sqrt(dx*dx+dy*dy)*dr;
				if (r>1 && p.w>.5) {
					let a=Math.atan2(dx,dy)-.707;
					for (let i=-1;i<=1;i+=2) {
					let ox=p.xe;
					let oy=p.ye; 
					let na=a+da*i;
					let nx=p.xe+r*(Math.cos(na)+Math.sin(na));
					let ny=p.ye+r*(-Math.sin(na)+Math.cos(na));
					q.push({xs:ox,ys:oy,xe:nx,ye:ny,w:p.w/2});
					}
				}
			}
	    }
	}
	update(dt) {
	    this.item.rotation.x+=dt*this.ds;
	    this.item.rotation.z+=dt*this.ds/2.;
		let e=Terrain.nearest(this.item.position);
		this.item.position.x+=5;
		let ex=Terrain.nearest(this.item.position);
		this.item.position.x-=5;
		this.item.position.z+=5;
		let ez=Terrain.nearest(this.item.position);
		this.item.position.z-=5;
		if (Math.abs(ez-e)>Math.abs(ex-e)) { // move in z axis
           if (ez<e) this.item.position.z+=0.1;
		   else this.item.position.z-=0.1;
		} else {
			if (ex<e) this.item.position.x+=0.1;
			else this.item.position.x-=0.1; 
		}
		this.item.position.y=Terrain.nearest(this.item.position)+this.size/2+.5;
		if (this.item.position.x<-100) this.item.position.x=100;
		if (this.item.position.x>100) this.item.position.x=-100;
		if (this.item.position.z<-100) this.item.position.z=100;
		if (this.item.position.z>100) this.item.position.z=-100;
		if (Common.prng()%10<1) {
			this.item.position.x+=Common.prng()%4-2;
			this.item.position.z+=Common.prng()%4-2;
		}
	}
};

class KRail {
	constructor(scene,x,y,z) {
		let geometry=new THREE.BoxGeometry(3,2,1);
		let canvas=document.getElementById('krailcanvas');
		KRail.draw(canvas);
		let texture = new THREE.CanvasTexture(canvas);
		let mat = new THREE.MeshBasicMaterial({map: texture});
        this.size=2;
		this.item=new THREE.Mesh(geometry,mat);
		let v=new THREE.Vector3(x,0.,z);
		v.y=Terrain.nearest(v);
		this.item.position.set(v.x,v.y+2,v.z);
	  this.item.rotation.y+=Common.fprng()*Math.PI;
		scene.add(this.item);
	}
	static drawn=false;
    static draw(canvas) {
		if(!KRail.drawn) {
		  let ctx=canvas.getContext('2d');
		  for (let x=0;x<64;x++)
		    for (let y=0;y<64;y++) {
				//let p=perlin(x,y);
				//console.log(p);
				let p=Common.fprng();
				let r=128*p+127;
				ctx.fillStyle=`rgba(${r},${r},${r},255)`;
				ctx.fillRect(x*4,y*4,4,4);
			}
		}
		KRail.drawn=true;
	}
	update(dt) {
	}
};

class Rock {
	constructor(scene,x,y,z,size) {
		this.size=size;
		let geometry=new THREE.SphereGeometry(this.size,16,8);
		let canvas=document.getElementById('rockcanvas');
		Rock.draw(canvas);
		let texture = new THREE.CanvasTexture(canvas);
		let mat = new THREE.MeshBasicMaterial({map: texture});
		this.item=new THREE.Mesh(geometry,mat);
		let v=new THREE.Vector3(x,0.,z);
		v.y=Terrain.nearest(v);
		this.item.position.set(v.x,v.y+size/2,v.z);
		this.item.rotation.y+=Common.fprng()*Math.PI;
		scene.add(this.item);
	}
	static drawn=false;
	static draw(canvas) {
		if(!Rock.drawn) {
			let ctx=canvas.getContext('2d');
			for (let x=0;x<128;x++)
			  for (let y=0;y<128;y++) {
				  //let p=perlin(x,y);
				  //console.log(p);
				  let p=Common.fprng();
				  let r=64*p+64;
				  ctx.fillStyle=`rgba(${r+16},${r},${r},255)`;
				  ctx.fillRect(x*2,y*2,2,2);
			  }
			Rock.drawn=true;
		  }  
	}
	update(dt) {
	}
};

class Trap {
	constructor(scene,x,y,z) {
		this.size=6;
		let canvas=document.getElementById('trapcanvas');
		Trap.draw(canvas);
		let texture = new THREE.CanvasTexture(canvas);
		let mat = new THREE.MeshBasicMaterial({map: texture});
		let geometry = new THREE.BoxGeometry(.5,6,.5);
		this.b1 = new THREE.Mesh( geometry, mat);
		this.b2 = new THREE.Mesh( geometry, mat);
		this.b3 = new THREE.Mesh( geometry, mat);
		this.item=new THREE.Group();
//		this.b1.rotation.x=Math.PI/3;
		this.item.add( this.b1 );
		this.b2.rotation.x=Math.PI/3;
		this.b2.rotation.y=Math.PI/4;
		this.item.add( this.b2 );
		this.b3.rotation.z=Math.PI/3;
		this.b3.rotation.y=-Math.PI/4;
		this.item.add( this.b3 );
		let v=new THREE.Vector3(x,0.,z);
		v.y=Terrain.nearest(v);
		this.item.position.set(v.x,v.y+2.5,v.z);
		this.item.rotation.y+=Common.fprng()*Math.PI;
		scene.add(this.item);
	}
	static drawn=false;
	static draw(canvas) {
		if(!Trap.drawn) {
			let ctx=canvas.getContext('2d');
			for (let x=0;x<32;x++)
			  for (let y=0;y<32;y++) {
				  //let p=perlin(x,y);
				  //console.log(p);
				  let p=Common.fprng();
				  let r=255*p;
				  ctx.fillStyle=`rgba(${r},${r/2},${r/2},255)`;
				  ctx.fillRect(x*8,y*8,8,8);
			  }
			  Trap.drawn=true;
		  }  
	}
	update(dt) {
	}
};

class Waypoint {
	constructor(scene,x,y,z,size) {
		this.size=size;
		let canvas=document.getElementById('waypointcanvas');
		Waypoint.draw(canvas);
		let texture = new THREE.CanvasTexture(canvas);
		let mat = new THREE.MeshBasicMaterial({map: texture});
		let geometry = new THREE.OctahedronGeometry(this.size);
		this.item = new THREE.Mesh( geometry, mat);
		let v=new THREE.Vector3(x,0.,z);
		v.y=Terrain.nearest(v);
		this.item.position.set(v.x,v.y+this.size,v.z);
		scene.add(this.item);
	}
	static drawn=false;
	static draw(canvas) {
		if(!Waypoint.drawn) {
			let ctx=canvas.getContext('2d');
			for (let x=0;x<512/16;x++)
			  for (let y=0;y<256/16;y++) {
				  //let p=perlin(x,y);
				  //console.log(p);
				  let p=Common.fprng();
				  let r=255*p;
				  ctx.fillStyle=`rgba(${r},${r},${r/4},255)`;
				  ctx.fillRect(x*16,y*16,16,16);
			  }
			  Waypoint.drawn=true;
		  }  
	}
	update(dt) {
		this.item.rotation.y+=dt*.01;
	}
};

class End {
	constructor(scene,x,y,z) {
		this.size=3;
		let canvas=document.getElementById('endcanvas');
		End.draw(canvas);
		let texture = new THREE.CanvasTexture(canvas);
		let mat = new THREE.MeshBasicMaterial({map: texture});
		let geometry = new THREE.DodecahedronGeometry(this.size);
		this.item = new THREE.Mesh( geometry, mat);
		let v=new THREE.Vector3(x,0.,z);
		v.y=Terrain.nearest(v);
		this.item.position.set(v.x,v.y+this.size,v.z);
		scene.add(this.item);
	}
	static drawn=false;
	static draw(canvas) {
		if(!End.drawn) {
			let ctx=canvas.getContext('2d');
			for (let x=0;x<512/16;x++)
			  for (let y=0;y<256/16;y++) {
				  //let p=perlin(x,y);
				  //console.log(p);
				  let p=Common.fprng();
				  let r=128*p+127;
				  ctx.fillStyle=`rgba(${r},0,0,255)`;
				  ctx.fillRect(x*16,y*16,16,16);
			  }
			  End.drawn=true;
		  }  
	}
	update(dt) {
		this.item.rotation.y+=dt*.01;
	}
};

class Start {
	constructor(scene,x,y,z) {
		this.size=3;
		let canvas=document.getElementById('startcanvas');
		Start.draw(canvas);
		let texture = new THREE.CanvasTexture(canvas);
		let mat = new THREE.MeshBasicMaterial({map: texture});
		let geometry = new THREE.DodecahedronGeometry(this.size);
		this.item = new THREE.Mesh( geometry, mat);
		let v=new THREE.Vector3(x,0.,z);
		v.y=Terrain.nearest(v);
		this.item.position.set(v.x,v.y+this.size,v.z);
		scene.add(this.item);
	}
	static drawn=false;
	static draw(canvas) {
		if(!Start.drawn) {
			let ctx=canvas.getContext('2d');
			for (let x=0;x<512/16;x++)
			  for (let y=0;y<256/16;y++) {
				  //let p=perlin(x,y);
				  //console.log(p);
				  let p=Common.fprng();
				  let r=128*p+127;
				  ctx.fillStyle=`rgba(0,${r},0,255)`;
				  ctx.fillRect(x*16,y*16,16,16);
			  }
			  Start.drawn=true;
		  }  
	}
	update(dt) {
		this.item.rotation.y+=dt*.01;
	}
};

export {Start,End,Waypoint,Rock,KRail,Tumbleweed,Trap};

