function getNote(note,octave) {
  let notes={C:4186.01/2,Db:4434.92/2,D:4698.64/2,
	Eb:4978.03/2,E:2637.02,F:2793.83,Gb:2959.96,G:3135.96,
	Ab:3322.44,A:3520.0,Bb:3729.31,B:3951.07};
  for (const [key, value] of Object.entries(notes))
	if (key==note) return value/Math.pow(2,7-octave);
}

let chords=new Map();
function addChord(chordName,notes) {
	chords.set(chordName,notes);
}
function getChordNotes(chordName) {
	if (chords.has(chordName)) return chords.get(chordName);
}

class R {
	constructor(time) {this.time=time;}
	playCore(audio,osc) {
		osc.type='sine';
		osc.frequency.value=1;
	}
	play(audio,after,speed) {
		if (speed==null) speed=1.0;
		this.speed=speed;
		let currentTime=audio.currentTime;
		let osc=audio.createOscillator();
		osc.connect(audio.destination);
		this.playCore(audio,osc);
		osc.start(currentTime);
		osc.stop(currentTime+this.time*this.speed);
		if (after!==null)
		  osc.addEventListener("ended",(event)=>{after.playNext()});
	}
};

class N extends R {
	constructor(note,octave,time) {
		super(time);
		this.note=note;
		this.octave=octave;
	}
	playCore(audio,osc) {
		osc.type="sine";
		osc.frequency.value=getNote(this.note,this.octave);
	}
};

class C extends R {
	constructor(chord,octave,time) {
		super(time);
		this.chord=chord;
		this.octave=octave;
	}
	playCore(audio,osc) {
		let currentTime=audio.currentTime;
		let notes=getChordNotes(this.chord);
		let oscs=[];
		for (let i=0;i<notes.length;i++) {
			if (i==0) oscs[i]=osc;
			else oscs[i]=audio.createOscillator();
		    oscs[i].type='sawtooth';
		    oscs[i].frequency.value=getNote(notes[i],this.octave);
			if (i>0){
		      oscs[i].connect(audio.destination);
			  oscs[i].start(currentTime);
			  oscs[i].stop(currentTime+this.time*this.speed);
			}
		}
	}
};

class Track {
	constructor(notesArray,repeat) {
		this.sequence=notesArray;
		this.current=0;
		this.playing=false;
		this.audio=null;
		if (repeat==null) repeat=1;
		this.repeat=repeat;
		this.remaining=0;
	}
	add(element) { this.sequence.push(element); }
	addTrack(other) { this.sequence=this.sequence.concat(other.sequence); }
	playNext() {
		if (!this.playing) return;
		this.current++;
		if (this.current<this.sequence.length) {
			this.sequence[this.current].play(this.audio,this,this.speed);
		} else if (this.remaining>0) {
			this.current=0;
			this.remaining--;
			this.sequence[this.current].play(this.audio,this,this.speed);
		} else {
			this.playing=false;
		}
	}
	play(audio,speed) {
		if (speed==null) speed=1;
		this.speed=speed;
		this.playing=true;
		this.audio=audio;
		this.remaining=this.repeat-1;
		this.current=0;
		this.sequence[this.current].play(this.audio,this,this.speed);
	}
	stop() {this.playing=false}
};

addChord('C',['C','E','G']);
addChord('G',['G','B','D']);
addChord('Am',['A','C','E']);
addChord('F',['F','A','C']);
addChord('Dm',['D','F','A']);

// Possible wave collapse to make "infinite series" of chords

//https://www.justinguitar.com/guitar-lessons/5-common-chord-progressions-bg-1011
let common=[];
common[0]=new Track([new C('C',4,.5),new C('G',4,.5), new C('Am',4,.5),new C('F',4,.5)],1);
common[1]=new Track([new C('Am',4,.5),new C('F',4,.5), new C('C',4,.5),new C('G',4,.5)],1);
common[2]=new Track([new C('C',4,.5),new C('F',4,.5), new C('G',4,.5),new C('F',4,.5)],1);
common[3]=new Track([new C('C',4,.5),new C('Am',4,.5), new C('F',4,.5),new C('C',4,.5)],1);
common[4]=new Track([new C('Dm',4,.5),new C('G',4,.5), new C('C',4,.5),new C('Am',4,.5)],1);

// https://people.carleton.edu/~jellinge/m101s12/Pages/16/16Ragtime/Ragtime1.html
const joplin=new Track([],1);
joplin.addTrack(common[0]); // Free
joplin.addTrack(common[1]); // Blend with m0
joplin.addTrack(common[3]); // Blend with m1
joplin.addTrack(common[2]); // Blend with m2
joplin.addTrack(common[0]); // repeat m0
joplin.addTrack(common[1]); // repeat m1
joplin.addTrack(common[4]); // Blend with m1 but not same as m0 m1
joplin.addTrack(common[1]); // Blend with m7
joplin.addTrack(common[0]); // repeat m0
joplin.addTrack(common[1]); // repeat m1
joplin.addTrack(common[3]); // repeat m2
joplin.addTrack(common[2]); // repeat m3
joplin.addTrack(common[4]); // Blend with m11
joplin.addTrack(common[3]); // Blend with m12
joplin.addTrack(common[2]); // Blend with m13
joplin.addTrack(common[1]); // Blend with m14 and m0 (for repeat)

export {joplin,common,Track,C,N,R};
