import * as Common from './common.js';

Common.addVS("terrain",Common.vert`
varying vec3 normalInterp;
varying vec3 vertPos;
varying vec3 newPosition;

void main(){
    newPosition=position;
    vec3 inputPosition=position;
    vec2 inputTexCoord=vec2(0.0,0.0);
    vec3 inputNormal=normal;

    mat4 projection=projectionMatrix;
    mat4 modelview=modelViewMatrix;
    mat4 normalMat=mat4 (normalMatrix);

    gl_Position = projection * modelview * vec4(inputPosition, 1.0);
    vec4 vertPos4 = modelview * vec4(inputPosition, 1.0);
    vertPos = vec3(vertPos4) / vertPos4.w;
    normalInterp = vec3(normalMat * vec4(inputNormal, 0.0));
}
`);