<?php
require 'db.php';
/* If a remote addr is requesting a hash it is probably trying to claim jump.  The
remote addr is not allowed to get a hash more than 1 time a minute.
The counter is reset every time a new hash request is made.
*/
$addr=$_SERVER["REMOTE_ADDR"];
$sql="select timestampdiff(MINUTE,time,NOW()) as last from requests where addr='$addr' limit 1";
$result=$conn->query($sql);
$sql="insert into requests (addr,time) values ('$addr',NOW())";
$result=$conn->query($sql);
$ok=false;
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    if ($row['last']>=1) $ok=true;
} else $ok=true;
if ($ok) {
    require 'hash.php';
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode(['hash'=>$hash]);    
} else {
  echo json_encode(['status'=>'too many requests']);;
}
?>