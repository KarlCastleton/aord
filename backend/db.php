<?php 
// Use environment variables don't include constants
$user=getenv("aorduser");
$pw=getenv("aordpw");
$db=getenv("aorddb");
$conn = new mysqli('localhost',$user,$pw,$db);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
/* create table queries

CREATE TABLE `highs` (
  `world` int,
  `distance` float,
  `time` float,
  `name` varchar(3)
)

CREATE TABLE `requests` (
  `addr` varchar(20),
  `time` timestamp,
  KEY `addr` (`addr`)
)

*/
?>