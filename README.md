# AORD

Autonomous Off-Road Delivery (AORD).  An entry into Ludum Dare 53.  

You are a delivery driver in the future with the worst route.  But in the future you
are not in the delivery vehicle you are back at the office and directing the "drone" delivery truck.  Your job is to set the controls for the vehicle such that it gets
to the destination quickly.  Your boss is a bit of a jerk so if you change the controls
after the vehicle is released you don't get credit for the race.

Choose a world, set the controls and see how you do.  Show your friends the high score
and let them see if they can do better. 

Nearly inifinite worlds to choose from (really on 2^32).  Each with unique terrain, plants, vehicle, course and even music. In general higher world numbers are harder terrains with more intense situations.

## Name
AORD Autonomous Off-Road Delivery.

## Description
Hopefully? A procedurally generated 3D autonomous vehicle race.  A combination of the real DARPA autonomous car challenges and race against your friends simulator.  The best part of participating in the DARPA autonomous car challenges was the "set it" and watch the results of a vehicle trying its best to make it to waypoints.  

Gamification of Autonomous Drive Algorithms
-You can set the given controls as you like and run the course.
-You can even change a control while driving but then your score will not count.
-Your score will be time to completion.  Failing completion it will be distance travel.

## Visuals
Adding these soon.

## Installation
This will sit on top of an old LAMP server (just the fastest way to get this up and running for me, not into learning a bunch of servless tools while trying to get this up and running).  Probably will put NGINX in front of connection to LAMP server, that is on this list for today.

## Usage
When installed properly the expecation is you will be able to run the game through a browser.  There is the expectation to make a back-end that keeps high scores for the top ten on any course. There is also and expectation to make it a Progessive Web Application so you can install and play offline then run your best runs for score when connected to a network.  

## Support
Comments to the project will be read during the Ludum Dare but will be responsed to after the Ludum Dare unless it is something pressing.  Just trying to keep my head down and code.

## Roadmap
- Day 0: Decide on idea and then cram it into Theme.
- Day 1: Infrastructure
- Day 2: Game Play

## Contributing
I am open to contributions but after Ludum Dare 53 is over. 

Documentation will be added after the bulk of activity is complete.  Currently very sparse documentation but plenty of straight forward code with good names for Objects (yes I am an OO programmer).  Good names for variables and fairly small functions. 

## Authors and acknowledgment
Karl Castleton a Associate Professor of Computer Science at Colorado Mesa University.  He is just checking to see if he still has the skills to get an application put together quickly.  

## License
GNU AFFERO GENERAL PUBLIC LICENSE (Version 3, 19 November 2007)

minus library js/three.js of course only external library

## Project status
About 24 hours into Ludum Dare 53.  First day is largely infrastructure to have the music procedurally generated,

- textures (canvas rendered then used as a texture, or glsl)
- courses (starting and ending points and course are generated)
- terrain (elevations will be generated using a fractal terrain generation technique)
- music (common chord progression with potentiall "Contraint" algorithm, maybe wave collapse)

and have a Psuedo Random Number Generator (prng) so that each level can be consistently generated across browsers and implementation.  Most time was spent working on a consistent way to have the glsl files loaded once and be in their own file with a .glsl extension so development tools can syntax highlight appropriately.